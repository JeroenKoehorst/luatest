﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ScriptRunner
{
	public static class DelegateHelper
	{
		/// <summary>
		///   The name of the Invoke method of a Delegate.
		/// </summary>
		const string InvokeMethod = "Invoke";

		/// <summary>
		///   Get method info for a specified delegate type.
		/// </summary>
		/// <param name = "delegateType">The delegate type to get info for.</param>
		/// <returns>The method info for the given delegate type.</returns>
		public static MethodInfo MethodInfoFromDelegateType(Type delegateType)
		{
			Contract.Requires<ArgumentException>(
			delegateType.IsSubclassOf(typeof(MulticastDelegate)),
			"Given type should be a delegate.");

			return delegateType.GetMethod(InvokeMethod);
		}

		/// <summary>
		///   Creates a delegate of a specified type that represents the specified
		///   static or instance method, with the specified first argument.
		///   Conversions are done when possible.
		/// </summary>
		/// <typeparam name = "T">The type for the delegate.</typeparam>
		/// <param name = "firstArgument">
		///   The object to which the delegate is bound,
		///   or null to treat method as static
		/// </param>
		/// <param name = "method">
		///   The MethodInfo describing the static or
		///   instance method the delegate is to represent.
		/// </param>
		public static T CreateCompatibleDelegate<T>(
			object firstArgument,
			MethodInfo method)
		{
			MethodInfo delegateInfo = MethodInfoFromDelegateType(typeof(T));

			ParameterInfo[] methodParameters = method.GetParameters();
			ParameterInfo[] delegateParameters = delegateInfo.GetParameters();

			// Convert the arguments from the delegate argument type
			// to the method argument type when necessary.
			ParameterExpression[] arguments =
			(from delegateParameter in delegateParameters
			 select Expression.Parameter(delegateParameter.ParameterType))
			.ToArray();
			Expression[] convertedArguments =
			new Expression[methodParameters.Length];
			for (int i = 0; i < methodParameters.Length; ++i)
			{
				Type methodType = methodParameters[i].ParameterType;
				Type delegateType = delegateParameters[i].ParameterType;
				if (methodType != delegateType)
				{
					convertedArguments[i] =
					Expression.Convert(arguments[i], methodType);
				}
				else
				{
					convertedArguments[i] = arguments[i];
				}
			}

			// Create method call.
			ConstantExpression instance = firstArgument == null
			? null
			: Expression.Constant(firstArgument);
			MethodCallExpression methodCall = Expression.Call(
			instance,
			method,
			convertedArguments
			);

			// Convert return type when necessary.
			Expression convertedMethodCall =
			delegateInfo.ReturnType == method.ReturnType
			? (Expression)methodCall
			: Expression.Convert(methodCall, delegateInfo.ReturnType);

			return Expression.Lambda<T>(
			convertedMethodCall,
			arguments
			).Compile();
		}
	}
}