﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ScriptRunner
{
	public class LuaFile
	{
		private string name;
		private FileInfo file;

		public LuaFile(string filePath)
		{
			this.file = new FileInfo(filePath);
			this.name = Path.GetFileNameWithoutExtension(file.Name);
		}

		public string Name => name;
		public FileInfo File => file;
	}
}