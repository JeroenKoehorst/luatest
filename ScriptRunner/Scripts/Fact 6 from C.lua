﻿function fact (n)
	if (n == 0) then
		return 1
	else
		-- Call a C# multiply function
		return CFunctions.Mul(n, fact(n - 1))
	end
end

function main ()
	return fact(6)
end