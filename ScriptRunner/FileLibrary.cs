﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;

namespace ScriptRunner
{
	public class FileLibrary
	{
		private readonly string path;
		private readonly string filter;

		public FileLibrary(string path, string filter)
		{
			this.path = path;
			this.filter = filter;
			Files = new ObservableCollection<LuaFile>();

			FileSystemWatcher fsWatcher = new FileSystemWatcher(path, filter);
			fsWatcher.Changed += FsWatcher_Changed;
			fsWatcher.Created += FsWatcher_Created;
			fsWatcher.Deleted += FsWatcher_Deleted;
			fsWatcher.Renamed += FsWatcher_Renamed;
			ReLoadFiles();
			fsWatcher.EnableRaisingEvents = true;
		}

		private void FsWatcher_Renamed(object sender, RenamedEventArgs e)
		{
			ReLoadFiles();
		}

		private void FsWatcher_Deleted(object sender, FileSystemEventArgs e)
		{
			ReLoadFiles();
		}

		private void FsWatcher_Created(object sender, FileSystemEventArgs e)
		{
			ReLoadFiles();
		}

		private void FsWatcher_Changed(object sender, FileSystemEventArgs e)
		{
			ReLoadFiles();
		}

		private void ReLoadFiles()
		{
			var fs = Directory.GetFiles(path, filter);

			Application.Current.Dispatcher.Invoke(() =>
			{
				Files.Clear();
				fs.ToList().ForEach(f => Files.Add(new LuaFile(f)));
			});
		}

		public ObservableCollection<LuaFile> Files;
	}
}