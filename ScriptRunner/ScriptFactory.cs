﻿using MoonSharp.Interpreter;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScriptRunner
{
	public static class ScriptFactory
	{
		public static Script New()
		{
			UserData.RegisterAssembly();
			Script script = new Script();
			script.Globals["CFunctions"] = new CFunctions();

			return script;
		}

		/*private static void LoadLuaFunctions(Script script, Type type)
		{
			foreach (MethodInfo method in type.GetMethods())
			{
				var ret = method.ReturnType;
				var args = method.GetGenericArguments();
				//script.Globals[method.Name] = Delegate.CreateDelegate(typeof(object), method);
				script.Globals[method.Name] = DelegateHelper.CreateCompatibleDelegate<Action<object>>(null, method);
			}
		}*/
	}
}