﻿using MoonSharp.Interpreter;
using System.Threading;

namespace ScriptRunner
{
	/// <summary>
	/// These functions will be automatically available in the LUA scripts.
	/// Call with classname as prefix: CFunctions.Mul(3, 3)
	/// </summary>
	[MoonSharpUserData]
	public class CFunctions
	{
		public int Mul(int a, int b)
		{
			return a * b;
		}

		public string RobotMoveTo(int x, int y, int z)
		{
			Thread.Sleep(2000);
			return $"Robot moved to {x}, {y}, {z}";
		}

		public int Wait(int ms)
		{
			Thread.Sleep(ms);
			return 0;
		}

		public void MessageBoxShow(string text)
		{
			System.Windows.MessageBox.Show(text);
		}
	}
}