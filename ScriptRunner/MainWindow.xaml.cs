﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using MoonSharp.Interpreter;

namespace ScriptRunner
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private FileLibrary scriptLibrary;

		public MainWindow()
		{
			InitializeComponent();
			scriptLibrary = new FileLibrary("Scripts", "*.lua");
			lvScripts.ItemsSource = scriptLibrary.Files;
		}

		private void Button_Click_Script(object sender, RoutedEventArgs e)
		{
			string filename = ((LuaFile)((Button)sender).DataContext).File.FullName;
			BackgroundWorker bg = new BackgroundWorker();
			bg.DoWork += Bg_DoWork;
			bg.RunWorkerAsync(filename);
		}

		private void Bg_DoWork(object sender, DoWorkEventArgs e)
		{
			RunLuaScript(e.Argument.ToString());
		}

		private void RunLuaScript(string filename)
		{
			string scriptText = File.ReadAllText(filename);

			WriteOutput("\nRunning script: " + filename);

			// create new LUA script
			Script script = ScriptFactory.New();

			// add number from View
			Application.Current.Dispatcher.Invoke(() => { script.Globals["input"] = input.Text; } );

			// run script
			try
			{
				script.DoString(scriptText);
				DynValue res = script.Call(script.Globals["main"]);
				WriteOutput("Scriptresult = " + res.ToString());
			}
			catch (Exception ex)
			{
				WriteOutput("Error: " + ex.Message);
			}
		}

		private void WriteOutput(string text)
		{
			Application.Current.Dispatcher.Invoke(() =>
			{ Output.Text += "\n" + text; }
			);
		}
	}
}